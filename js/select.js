/**/
var selectMultiplie = function(){
    var obj = {};
    var $select, $count;
    var default_label = '(Все)';

    obj.init = function(){
        $(".select.select_multiplie .select__trigger").on('click', click);
        $(document).on('mouseup', clickOutside);
        updateCountAll();
    };

    var click = function(){
        $select = $(this).closest('.select.select_multiplie');
        $count = $select.find(".select__label-value");
        if(!$select.hasClass("select_active")){
            closeAll();
        }
        $select.toggleClass('select_active');
        updateCount();
    };

    var clickOutside = function(e){
        if ($select != undefined && !$select.is(e.target) && $select.has(e.target).length === 0){
            $select.removeClass('select_active');
            updateCount();
        }
    }

    var updateCount = function(){
        if(!$select.hasClass('select_active') && $select.find(":checked").length > 0){
            $count.text("("+$select.find(":checked").length+")");
        }else{
            if($select.find(":checked").length < 1)
                $count.text(default_label);
        }
    };

    var updateCountAll = function(){
        $('.select.select_multiplie').each(function(){
            var count = $(this).find(":checked").length;
            if(count > 0){
                $(this).find(".select__label-value").text("("+count+")");
            }else{
                $(this).find(".select__label-value").text(default_label);
            }
        });
    };

    var closeAll = function(){
        $(".select.select_active").removeClass('select_active');
        updateCountAll();
    };

    return obj;

}();



/**/

var selectSimple = function(){
    var obj = {};
    var $select, $value;

    obj.init = function(){
        $(".select.select_simple .select__trigger").on('click', click);
        $(".select.select_simple .select__item").on('click', clickItem);
        $(document).on('mouseup', clickOutside);
        updateValueAll();
    };

    var click = function(){
        $select = $(this).closest('.select.select_simple');
        $value = $select.find(".select.select__label-value");
        if(!$select.hasClass("select_active")){
            closeAll();
        }
        $select.toggleClass('select_active');
        updateValue();
    };

    var clickItem = function(){
        $(".select.select-simple.select_active").removeClass('select_active');
        updateValue();
    };

    var clickOutside = function(e){
        if ($select != undefined && !$select.is(e.target) && $select.has(e.target).length === 0){
            $select.removeClass('select_active');
            updateValue();
        }
    }

    var updateValue = function(){
        var $selected = $select.find(":checked");
        if($selected.length > 0){
            $value.html(getSelectedHtml($selected));
        }else{
            //$value.html("");
        }
    };

    var updateValueAll = function(){
        $('.select.select_simple').each(function(){
            var $selected = $(this).find(":checked");
            if($selected.length > 0){
                $(this).find(".select__label-value").html(getSelectedHtml($selected));
            }
        });
    };

    var closeAll = function(){
        $(".select.select__active").removeClass('select_active');
        updateValueAll();
    };

    var getSelectedHtml = function($selected){
        var $label = $selected.closest("label");
        var fa = $label.find('.fa').get(0);
        if(fa != undefined){
            return " ("+$label.text()+" "+fa.outerHTML+")";
        }
        return " ("+$label.text()+")";
    }

    return obj;

}();