
setSiteMinWidth(445);

$(".select, input[type='checkbox']").styler();

$("button[data-href]").on("click", function(){ window.location.href = $(this).data('href'); });

$(".slider__carousel").preload($(".slider"), $(".slider__slide img"), function(){ $(this).carousel(); });

$(".js-collapse").on("click", function(){ $($(this).toggleClass('active').data('target')).slideToggle(); });

selectMultiplie.init();

selectSimple.init();

searchCleaner.init();

priceSlider.init();

cart.init();