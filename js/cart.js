var cart = function(){
	var obj = {};

	var i_container = '.js-cart-item'; //item container
	var i_image = '.js-cart-image';	//item image
	var i_add = '.js-cart-add'; //item add action
	var i_qty = '.js-cart-qty'; //item qty fiels
	var i_pid = '.js-cart-pid'; //item pid field
	var c_container = ".cartbox"; //cart container
	var c_count = '.cartbox__count'; //cart count  container

	obj.init = function(){
		$(i_add).on('click', add);
	};

	var add = function(){
		var $this = $(this);
		var pid = parseInt($this.closest(i_container).find(i_pid).val());
		var qty = parseInt($this.closest(i_container).find(i_qty).val());
		var $image = $this.closest(i_container).find(i_image);
		var doAnimateImage = $this.data('cart-animate') == undefined || $this.data('cart-animate') == true ? true : false;
		var data = {
			action: 'doAddToCart',
			pid: pid,
			qty: qty
		};
		if($image.length > 0 && doAnimateImage){
			animateImage($image);
		}
		send(data, function(result){
			if(result != ''){
				updateView();
			}
		});
	};

	var updateView = function(){
		var data = {
			action: 'updateCartCount'
		};
		send(data, function(result){
			$(c_count).text(result.count);
		});
	};

	var send = function(data, callback){
		$.ajax({
			url: "./",
			data: data,
			dataType: "json",
			success: callback,
			error: function(error){
				console.log(error);
			}
		});
	};

	var animateImage = function($image){
		var position_top = $image.offset().top,
			position_left = $image.offset().left,
			position_width = $image.outerWidth(),
			position_height = $image.outerHeight(),
			$target_cart = $(c_container),
			target_top = $target_cart.offset().top,
			target_left = $target_cart.offset().left,
			target_width = $target_cart.outerWidth(),
			target_height = $target_cart.outerHeight();
			var src;
			if($image.get(0).tagName != "IMG"){
				src = $image.find('img').attr('src');
			}else{
				src = $image.attr('src');
			}
			$('body').append("<div class='fly_cart' style='position: absolute; top:"+position_top+"px; left:"+position_left+"px; width:"+position_width+"px; height:"+position_height+"px; background: url("+src+") center center no-repeat; background-size: contain; z-index:100000; font-size: 20px; color: #fff;text-align: center;' class='text-center'><span class='icon icon-buy'></span></div>");
			$('.fly_cart').animate({
				"left": target_left+"px",
				"top": target_top+"px",
				"opacity": 0.3,
				"width": target_width+"px",
				"height": target_height+"px",
				"border-radius": "10px" 
			}, 1500).queue(function(n){
				$(this).remove();
				n();
			});
	};

	return obj;
}();