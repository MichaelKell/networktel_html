searchCleaner = function() {
    var obj = {};
    var $el = $('.search__field');
    var $cleaner = $(".search__cleaner");
    obj.init = function() {
        $el.on("keyup focus blur", function() {
            if (checkLength()) {
                $cleaner.addClass('search__cleaner_active');
            } else {
                $cleaner.removeClass('search__cleaner_active');
            }
        });
        $cleaner.on("click", function() {
            $el.val("");
            $cleaner.removeClass("search__cleaner_active");
        });
    };
    var checkLength = function() {
        return $el.val().length > 0 ? true : false;
    };
    return obj;
}();
