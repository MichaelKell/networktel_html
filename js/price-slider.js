/* слайдер цен */
var priceSlider = function(){
	var obj = {};
	var slider = $(".price-slider__slider").get(0);
	var minField = $(".price-slider").find('input[name="PriceMin"]').get(0);
	var maxField = $(".price-slider").find('input[name="PriceMax"]').get(0);
	var inputs = [minField, maxField];
	var min, max;

	obj.init = function(){
		if(slider === undefined){
			return;
		}
		min = $(".price-slider__slider").data("min");
		max = $(".price-slider__slider").data("max");
		noUiSlider.create(slider, {
			start: [minField.value, maxField.value],
			connect: true,
			range: {
				'min': min,
				'max': max
			}
		});
		slider.noUiSlider.on('update', function( values, handle ) {
			inputs[handle].value = parseInt(values[handle]);
		});
		attachEvents();
	};

	var attachEvents = function(){
		inputs.forEach(function(input, handle) {
			input.addEventListener('change', function(){
				setSliderHandle(handle, this.value);
			});
			input.addEventListener('keydown', function( e ) {
				var values = slider.noUiSlider.get();
				var value = Number(values[handle]);
				var steps = slider.noUiSlider.steps();
				var step = steps[handle];
				var position;
				switch ( e.which ) {
					case 13:
						setSliderHandle(handle, this.value);
						break;
					case 38:
						position = step[1];
						if ( position === false ) {
							position = 1;
						}
						if ( position !== null ) {
							setSliderHandle(handle, value + position);
						}
						break;
					case 40:
						position = step[0];
						if ( position === false ) {
							position = 1;
						}
						if ( position !== null ) {
							setSliderHandle(handle, value - position);
						}
						break;
				}
			});
		});
	}

	var setSliderHandle = function(i, value) {
		var r = [null,null];
		r[i] = value;
		slider.noUiSlider.set(r);
	};

	return obj;
}();