'use strict';

var gulp = require('gulp');	
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var livereload = require('gulp-livereload');
var autoprefixer = require('gulp-autoprefixer');
//var order = require("gulp-order");

var sources = [
  './scss/main.scss',
  './scss/helpers.scss',
  './scss/forms.scss',
	'./scss/panel.scss',
  './scss/header.scss',
  './scss/nav.scss',
  './scss/promo.scss',
  './scss/catalogue-index.scss',
  './scss/product.scss',
  './scss/paginator.scss',
  './scss/filter.scss',
  './scss/why-us.scss',
  './scss/news.scss',
  './scss/info-index.scss',
  './scss/menu.scss',
	'./scss/footer.scss'
];

gulp.task('styles', function() {
  return gulp.src(sources)
  	.pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
	  .pipe(autoprefixer({browsers: ['last 5 versions']}))
    .pipe(concat('style.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./css'))
    .pipe(livereload());
});

gulp.task('default', ['styles']);

gulp.task('watch', function () {
  livereload.listen();
  gulp.watch('./scss/*.scss', ['default']);
});